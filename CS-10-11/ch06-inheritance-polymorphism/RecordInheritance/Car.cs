namespace RecordInheritance;

public record Car {
	public string Make { get; init; }
	public string Model { get; init; }
	public string Color { get; init; }

	public Car (string make, string model, string color) {
		Make = make;
		Model = model;
		Color = color;
	}

    public sealed override string ToString()
    {
        return $"This is a {Color} {Make} {Model}";
    }
}