namespace ObjectOverrides;

// Remember, Person extends Object.
class Person {
	public string FirstName { get; set; } = "";
	public string LastName { get; set; } = "";
	public int Age { get; set; }
	public string SSN { get; } = "";

	public Person (string fName, string lName, int personAge, string ssn) {
		FirstName = fName;
		LastName = lName;
		Age = personAge;
		SSN = ssn;
	}
	public Person() {}

    public override string ToString() 
		=> $"[First Name: {FirstName}; Last Name: {LastName}; Age: {Age};]";
	
	public override bool Equals (object obj) => obj?.ToString() == ToString();

	// Return a hash code based on unique string data.
	public override int GetHashCode() => SSN.GetHashCode();
	//OR...
	//public override int GetHashCode() => ToString().GetHashCode();
}