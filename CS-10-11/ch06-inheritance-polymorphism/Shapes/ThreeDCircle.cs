// This class extends Circle and hides the inherited Draw() method.
using Shapes;

class ThreeDCircle : Circle {

	public new string PetName { get; set; }
	public new void Draw() {
		Console.WriteLine("Drawing a 3D Circle");
	}
}