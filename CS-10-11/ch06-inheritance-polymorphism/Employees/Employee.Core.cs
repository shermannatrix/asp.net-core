namespace Employees;

partial class Employee {
    // Protected members.
    protected string _empName = "";
	protected int _empId;
	protected float _currPay;
	protected int _empAge;
	protected string _empSSN = "";
	protected DateTime _hireDate;
	protected BenefitPackage EmpBenefits = new BenefitPackage();
}