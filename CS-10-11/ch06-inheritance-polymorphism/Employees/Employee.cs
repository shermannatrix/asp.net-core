namespace Employees;

public enum EmployeePayTypeEnum {
	Hourly,
	Salaried,
	Commission
}

partial class Employee {
	
	public DateTime HireDate {
		get => _hireDate;
		set => _hireDate = value;
	}

	private EmployeePayTypeEnum _payType;
	public EmployeePayTypeEnum PayType {
		get => _payType;
		set => _payType = value;
	}

	// Updated Constructors.
	public Employee() {}
	public Employee(string name, int id, float pay, string ssn, DateTime hireDate) 
		: this(name, 0, id, pay, ssn, EmployeePayTypeEnum.Salaried, hireDate) { }
	public Employee (string name, int age, int id, float pay, string ssn, EmployeePayTypeEnum payType, DateTime hireDate) {
		// This reduces the amount of duplicate error checks.
		Name = name;
		Id = id;
		Pay = pay;
		Age = age;
		SocialSecurityNumber = ssn;
		PayType = payType;
		HireDate = hireDate;
	}

	// Add to the Employee base class.
	public Employee (string name, int age, int id, float pay, string empSsn, 
		EmployeePayTypeEnum payType) {
		Name = name;
		Id = id;
		Age = age;
		Pay = pay;
		SocialSecurityNumber = empSsn;
		PayType = payType;
	}

	// Propeties!
	public string Name {
		get { return _empName; }
		set {
			if (value.Length > 15) {
				Console.WriteLine("Error! Name length exceeds 15 characters!");
			} else {
				_empName = value;
			}
		}
	}

	// We could add additional business rules to the sets of these properties;
	// however, there is no need to do so for this example.
	public int Id {
		get { return _empId; }
		set { _empId = value; }
	}

	public float Pay {
		get { return _currPay; }
		set { _currPay = value; }
	}

	public int Age {
		// Properties as Expression-Bodied Members (New 7.0)
		get => _empAge;
		set => _empAge = value;
	}

	public string SocialSecurityNumber {
		get => _empSSN;
		private set => _empSSN = value;
	}

	// Expose certain benefit behaviors of object.
	public double GetBenefitCost() => EmpBenefits.ComputePayDeduction();

	// Expose object through a custom property.
	public BenefitPackage Benefits {
		get { return EmpBenefits; }
		set { EmpBenefits = value; }
	}

	// Methods.
	public virtual void GiveBonus (float amount) {
		Pay += amount;
		// Pay = this switch {
		// 	{Age: >= 18, PayType: EmployeePayTypeEnum.Commission, HireDate.Year: > 2020 }
		// 		=> Pay += .10F * amount,
		// 	{Age: >= 18, PayType: EmployeePayTypeEnum.Hourly, HireDate.Year: > 2020 }
		// 		=> Pay += 40F * amount/2080F,
		// 	{ Age: >= 18, PayType: EmployeePayTypeEnum.Salaried, HireDate.Year: > 2020 }
		// 		=> Pay += amount,
		// 	_ => Pay += 0
		// };
	}

	// Updated 
	public virtual void DisplayStats() {
		Console.WriteLine("Name: {0}", Name);
		Console.WriteLine("ID: {0}", Id);
		Console.WriteLine("Age: {0}", Age);
		Console.WriteLine("Pay: {0}", Pay);
		Console.WriteLine("SSN: {0}", SocialSecurityNumber);
	}
}