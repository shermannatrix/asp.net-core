namespace ProcessMultiException;

class Radio {
	public void TurnOn (bool on) {
		Console.WriteLine(on ? "Jamming" : "Quiet time...");
	}
}

class Car {
	// Constant for maximum speed.
	public const int MaxSpeed = 100;

	// Car properties.
	public int CurrentSpeed { get; set; } = 0;
	public string PetName { get; set; } = "";

	// Is the car still operational?
	private bool _carIsDead;

	// A car has-a radio.
	private readonly Radio _theMusicBox = new Radio();

	// Constructors
	public Car() {}
	public Car(string name, int speed) {
		CurrentSpeed = speed;
		PetName = name;
	}

	public void CrankTunes (bool state) {
		// Delegate request to inner object.
		_theMusicBox.TurnOn(state);
	}

	// See if Car has overheated.
	public void Accelerate(int delta) {
		if (delta < 0) {
			throw new ArgumentOutOfRangeException(nameof(delta),
				"Speed must be greater than zero");
		}

		if (_carIsDead) {
			Console.WriteLine($"{PetName} is out of order...");
		} else {
			CurrentSpeed += delta;
			if (CurrentSpeed >= MaxSpeed) {
				
				CurrentSpeed = 0;
				_carIsDead = true;

				// Use the "throw" keyword to raise an exception.
				throw new CarIsDeadException(
					"You have a lead foot.", DateTime.Now, 
					$"{PetName} has overheated!") {
					HelpLink = "http://www.CarsRUs.com"
				};
			} else {
				Console.WriteLine($"=> CurrentSpeed = {CurrentSpeed}");
			}
		}
	}
}