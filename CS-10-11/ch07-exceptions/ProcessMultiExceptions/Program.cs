﻿using ProcessMultiException;

Console.WriteLine("***** Handling Multiple Exceptions *****");
Car myCar = new Car("Rusty", 90);
try {
	// Trip Arg out of range exception.
	myCar.Accelerate(100);
} catch (CarIsDeadException e) 
	when (e.ErrorTimeStamp.DayOfWeek != DayOfWeek.Friday) {
	try {
		FileStream fs = File.Open(@"./carErrors.txt", FileMode.Open);
	} catch (Exception e2) {
		throw new CarIsDeadException(e.CauseOfError, e.ErrorTimeStamp, e.Message, e2);
	}
} catch (ArgumentOutOfRangeException e) {
	Console.WriteLine(e.Message);
} finally {
	// This will always occur. Exception or not.
	myCar.CrankTunes(false);
}

Console.ReadLine();