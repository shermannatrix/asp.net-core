﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("***** My First C# App *****");
Console.WriteLine("Hello, World!");
Console.WriteLine();
// Process any incoming args.
// foreach (string arg in args) {
// 	Console.WriteLine("Arg: {0}", arg);
// }

// Getting arguments using System.Environment.
// string[] theArgs = Environment.GetCommandLineArgs();
// foreach(string arg in theArgs) {
// 	Console.WriteLine("Arg: {0}", arg);
// }

// Additional members of the System.Environment class (updated 10.0)
static void ShowEnvironmentDetails() {
	// Print out the drives on this machine, and other interesting details.
	foreach(string drive in Environment.GetLogicalDrives()) {
		Console.WriteLine("Drive: {0}", drive);
	}

	Console.WriteLine("OS: {0}", Environment.OSVersion);
	Console.WriteLine("Number of processors: {0}", Environment.ProcessorCount);
	Console.WriteLine(".NET Core Version: {0}", Environment.Version);
}

ShowEnvironmentDetails();

// Wait for Enter key to be pressed before shutting down.
Console.ReadLine();