﻿using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;

Console.WriteLine("***** Fun with Basic Data Types *****");

LocalVarDeclarations();
DefaultDeclarations();
NewingDataTypes();
NewingDataTypesWith9();
ObjectFunctionality();
DataTypeFunctionality();

Console.WriteLine("=> Members of System.Boolean:");
Console.WriteLine("bool.FalseString: {0}", bool.FalseString);
Console.WriteLine("bool.TrueString: {0}", bool.TrueString);
Console.WriteLine();

CharFunctionality();
ParseFromStrings();
TryParseFromStrings();
UseDatesAndTimes();
// UseBigInteger();		// Won't run on Raspberry Pi 4B
DigitSeparators();
BinaryLiterals();
BasicStringFunctionality();
StringConcatenation();
EscapeChars();
StringInterpolation();
SIWithDefaultHandler();
StringEquality();
StringEqualityWithCompareRules();
StringsAreImmutable();
FunWithStringBuilder();
ProcessBytes();

Console.WriteLine("***** Fun with type conversions *****");

// Add two shorts and print the result.
short numb1 = 9, numb2 = 10;
Console.WriteLine("{0} + {1} = {2}", numb1, numb2, Add(numb1, numb2));

numb1 = 30000; 
numb2 = 30000;

// Explicitly cast the int into a short (and allow loss of data).
short answer = (short) Add(numb1, numb2);

Console.WriteLine("{0} + {1} = {2}", numb1, numb2, answer);

NarrowingAttempt();

Console.ReadLine();

static void LocalVarDeclarations() {
	Console.WriteLine("=> Data Declarations:");
	// local variables are declared as so:
	// dataType varName;
	// For initial values:
	// dataType varName = initialValue;
	int myInt = 8;
	string myString = "hello";

	// Declaring 3 bools on a single line.
	bool b1 = true, b2 = false, b3 = b1;

	// Use System.Boolean data type to declare a bool.
	System.Boolean b4 = false;

	Console.WriteLine("Your data, {0}, {1}, {2}, {3}, {4}, {5}", myInt,
		myString, b1, b2, b3, b4);

	Console.WriteLine();
}

static void DefaultDeclarations() {
	Console.WriteLine("=> Default Declarations:");
	int myInt = default;
	Console.WriteLine(myInt);
	Console.WriteLine();
}

static void NewingDataTypes() {
	Console.WriteLine("=> Using new to create variables:");
	bool b = new bool();			// Set to false
	int i = new int();				// Set to 0.
	double d = new double();		// Set to 0.
	DateTime dt = new DateTime();	// Set to 1/1/0001 12:00:00 AM
	Console.WriteLine("{0}, {1}, {2}, {3}", b, i, d, dt);
	Console.WriteLine();
}

// C# 9 added a shortcut for creating variable instances.
static void NewingDataTypesWith9() {
	Console.WriteLine("=> Using new to create variables:");
	bool b = new();
	int i = new();
	double d = new();
	DateTime dt = new();
	Console.WriteLine("{0}, {1}, {2}, {3}", b, i, d, dt);
	Console.WriteLine();
}

static void ObjectFunctionality() {
	Console.WriteLine("=> System.Object Functionality:");

	// A C# int is really a shorthand for System.Int32,
	// which inherits the following members from System.Object.
	Console.WriteLine("12.GetHashCode() = {0}", 12.GetHashCode());
	Console.WriteLine("12.Equals(23) = {0}", 12.Equals(23));
	Console.WriteLine("12.ToString() = {0}", 12.ToString());
	Console.WriteLine("12.GetType() = {0}", 12.GetType());
	Console.WriteLine();
}

static void DataTypeFunctionality() {
	Console.WriteLine("=> Data type Functionality:");

	Console.WriteLine("Max of int: {0}", int.MaxValue);
	Console.WriteLine("Min of int: {0}", int.MinValue);
	Console.WriteLine("Max of double: {0}", double.MaxValue);
	Console.WriteLine("Min of double: {0}", double.MinValue);
	Console.WriteLine("double.Epsilon: {0}", double.Epsilon);
	Console.WriteLine("double.PositiveInfinity: {0}", double.PositiveInfinity);
	Console.WriteLine("double.NegativeInfinity: {0}", double.NegativeInfinity);
	
	Console.WriteLine();
}

static void CharFunctionality() {
	Console.WriteLine("=> chat type Functionality:");

	char myChar = 'a';
	Console.WriteLine("char.IsDigit('a'): {0}", char.IsDigit(myChar));
	Console.WriteLine("char.IsLetter('a'): {0}", char.IsLetter(myChar));
	Console.WriteLine("char.IsWhiteSpace('Hello there', 5): {0}",
		char.IsWhiteSpace("Hello There", 5));
	Console.WriteLine("char.IsWhiteSpace('Hello there', 6): {0}",
		char.IsWhiteSpace("Hello There", 6));
	Console.WriteLine("char.IsPunctuation('?'): {0}", 
		char.IsPunctuation('?'));
	
	Console.WriteLine();
}

static void ParseFromStrings() {
	Console.WriteLine("=> Data type parsing:");
	bool b = bool.Parse("True");
	Console.WriteLine("Value of b: {0}", b);
	double d = double.Parse("99.884");
	Console.WriteLine("Value of d: {0}", d);
	int i = int.Parse("8");
	Console.WriteLine("Value of i: {0}", i);
	char c = Char.Parse("w");
	Console.WriteLine("Value of c: {0}", c);
	Console.WriteLine();
}

static void TryParseFromStrings() {
	Console.WriteLine("=> Data type parsing with TryParse:");
	if (bool.TryParse("True", out bool b)) {
		Console.WriteLine("Value of b: {0}", b);
	} else {
		Console.WriteLine("Default value of b: {0}", b);
	}

	string value = "Hello";
	if (double.TryParse(value, out double d)) {
		Console.WriteLine("Value of d: {0}", d);
	} else {
		Console.WriteLine("Failed to convert the input ({0}) to a " + 
			"double and the variable was assigned the default {1}",
			value, d);
	}
	Console.WriteLine();
}

static void UseDatesAndTimes() {
	Console.WriteLine("=> Dates and Times:");

	// This constructor takes (year, month, day)
	DateTime dt = new DateTime(2024, 5, 17);

	// What day of the month is this?
	Console.WriteLine("The day of {0} is {1}.", dt.Date, dt.DayOfWeek);

	// Month is now July
	dt = dt.AddMonths(2);
	Console.WriteLine("Daylight savings: {0}", dt.IsDaylightSavingTime());

	// This constructor takes (hours, minutes, seconds).
	TimeSpan ts = new TimeSpan(4, 30, 0);
	Console.WriteLine(ts);

	// Subtract 15 minutes from the current TimeSpan and print the result.
	Console.WriteLine(ts.Subtract(new TimeSpan(0, 15, 0)));

	DateOnly d = new DateOnly(2023, 9, 15);
	Console.WriteLine(d);

	TimeOnly t = new TimeOnly(13, 30, 0, 0);
	Console.WriteLine(t);

	Console.WriteLine();
}

/* static void UseBigInteger() {
	Console.WriteLine("=> Use Big Integer:");
	BigInteger biggy = BigInteger.Parse("999999999999999999999999999999999");
	Console.WriteLine("Value of biggy is {0}", biggy);
	Console.WriteLine("Is biggy an even value?: {0}", biggy.IsEven);
	Console.WriteLine("Is biggy a power of two?: {0}", biggy.IsPowerOfTwo);
	UseBigInteger reallyBig = BigInteger.Multiply(biggy, 
		BigInteger.Parse("8888888888888888888888888888888888888"));
	Console.WriteLine("Value of reallyBig is {0}", reallyBig);
	Console.WriteLine();
} */

static void DigitSeparators() {
	Console.WriteLine("=> Use DigitSeparators:");
	Console.Write("Integer:");
	Console.WriteLine(123_456);
	Console.Write("Long:");
	Console.WriteLine(123_456_789L);
	Console.Write("Float:");
	Console.WriteLine(123_456.1234F);
	Console.Write("Double:");
	Console.WriteLine(123_456.12);
	Console.Write("Decimal:");
	Console.WriteLine(123_456.12M);
	// Updated in 7.2, Hex can begin with _
	Console.Write("Hex:");
	Console.WriteLine(0x_00_00_FF);

	Console.WriteLine();
}

static void BinaryLiterals() {
	// Updated in 7.2, Binary can begin with _
	Console.WriteLine("=> Use Binary Literals:");
	Console.WriteLine("Sixteen: {0}", 0b_0001_0000);
	Console.WriteLine("Thirty Two: {0}", 0b_0010_0000);
	Console.WriteLine("Sixty Four: {0}", 0b_0100_0000);

	Console.WriteLine();
}

static void BasicStringFunctionality() {
	Console.WriteLine("=> Basic String functionality:");
	string firstName = "Weizhi";
	Console.WriteLine("Value of firstName: {0}", firstName);
	Console.WriteLine("firstName has {0} characters.", firstName.Length);
	Console.WriteLine("firstName in uppercase: {0}", firstName.ToUpper());
	Console.WriteLine("firstName in lowercase: {0}", firstName.ToLower());
	Console.WriteLine("firstName contains the letter y?: {0}", firstName.Contains("y"));
	Console.WriteLine("New first name: {0}", firstName.Replace("iz", "i z"));
	Console.WriteLine();
}

static void StringConcatenation() {
	Console.WriteLine("=> String concatenation:");
	string s1 = "Programming the ";
	string s2 = "PsychoDrill (PTP)";
	//string s3 = s1 + s2;
	string s3 = String.Concat(s1, s2);
	Console.WriteLine(s3);
	Console.WriteLine();
}

static void EscapeChars() {
	Console.WriteLine("=> Escape characters:");
	string strWithTabs = "Model\tColor\tSpeed\tPet Name ";
	Console.WriteLine(strWithTabs);

	Console.WriteLine("Everyone loves \"Hello World\"");
	Console.WriteLine("C:\\MyApp\\bin\\Debug");

	// Adds a total of 4 blank lines (3 escaped, 1 from WriteLine).
	//Console.WriteLine("All finished.\n\n\n");
	Console.WriteLine("All finished for real this time.{0}{0}{0}", Environment.NewLine);
	Console.WriteLine();
}

static void StringInterpolation() {
	Console.WriteLine("=> String interpolation:\a");

	// Some local variables we will plug into our larger string
	int age = 4;
	string name = "Soren";

	// using curly-bracket syntax.
	string greeting = string.Format("Hello {0} you are {1} years old.",
		name, age);
	Console.WriteLine(greeting);

	string greeting2 = $"Hello {name} you are {age} years old.";
	Console.WriteLine(greeting2);

	Console.WriteLine();
}

static void SIWithDefaultHandler() {
	Console.WriteLine("=> String interpolation under the covers:\a");
	int age = 4;
	string name = "Soren";

	var builder = new DefaultInterpolatedStringHandler(3, 2);
	builder.AppendLiteral("\tHello ");
	builder.AppendFormatted(name);
	builder.AppendLiteral(" you are ");
	builder.AppendFormatted(age);
	builder.AppendLiteral(" years old.");
	var greeting = builder.ToStringAndClear();
	Console.WriteLine(greeting);
	Console.WriteLine();
}

static void StringEquality() {
	Console.WriteLine("=> String equality:");
	string s1 = "Hello!";
	string s2 = "Yo!";

	Console.WriteLine("s1 = {0}", s1);
	Console.WriteLine("s2 = {0}", s2);
	Console.WriteLine();

	// Test these strings for equality.
	Console.WriteLine("s1 == s2: {0}", s1 == s2);
	Console.WriteLine("s1 == Hello!: {0}", s1 == "Hello!");
	Console.WriteLine("s1 == HELLO!: {0}", s1 == "HELLO!");
	Console.WriteLine("s1 == hello!: {0}", s1 == "hello!");
	Console.WriteLine("s1.Equals(s2): {0}", s1.Equals(s2));
	Console.WriteLine("Yo!.Equals(s2): {0}", "Yo!".Equals(s2));
	Console.WriteLine();
}

static void StringEqualityWithCompareRules() {
	Console.WriteLine("=> String Equality (Case Insensitive)");
	string s1 = "Hello!";

	string s2 = "HELLO!";
	Console.WriteLine("s1 = {0}", s1);
	Console.WriteLine("s2 = {0}", s2);
	Console.WriteLine();

	// Check the results of changing the default compare rules.
	Console.WriteLine("Default rules: s1={0}, s2={1} | s1.Equals(s2): {2}", s1, s2, s1.Equals(s2));
	Console.WriteLine("Ignore case: s1.Equals(s2, StringComparison.OrdinalIgnoreCase): {0}",
		s1.Equals(s2, StringComparison.OrdinalIgnoreCase));
	Console.WriteLine("Ignore case, Invariant Culture: s1.Equals(s2, StringComparison.InvariantCultureIgnoreCase): {0}",
		s1.Equals(s2, StringComparison.InvariantCultureIgnoreCase));
	Console.WriteLine();
	Console.WriteLine("Default rules: s1={0}, s2={1} | s1.IndexOf(\"E\"): {2}", 
		s1, s2, s1.IndexOf("E"));
	Console.WriteLine("Ignore case: s1.IndexOf(\"E\", StringComparison.OrdinalIgnoreCase): {0}",
		s1.IndexOf("E", StringComparison.OrdinalIgnoreCase));
	Console.WriteLine("Ignore case, Invariant Culture: s1.IndexOf(\"E\", StringComparison.InvariantCultureIgnoreCase): {0}",
		s1.IndexOf("E", StringComparison.InvariantCultureIgnoreCase));
	
	Console.WriteLine();
}

static void StringsAreImmutable() {
	Console.WriteLine("=> Immutable Strings:\a");
	// Set initial string value.
	string s1 = "This is my string.";
	Console.WriteLine("s1 = {0}", s1);

	// Uppercase s1?
	string upperString = s1.ToUpper();
	Console.WriteLine("upperString = {0}", upperString);

	// Nope! s1 is in the same format!
	Console.WriteLine("s1 = {0}", s1);

	Console.WriteLine();
}

static void FunWithStringBuilder() {
	Console.WriteLine("=> Using the StringBuilder:");
	StringBuilder sb = new StringBuilder("**** Fantastic Games ****");
	sb.Append("\n");
	sb.Append("\nHalf Life");
	sb.Append("\nMorrowind");
	sb.Append("\nDeus Ex 2");
	sb.Append("\nSystem Shock");
	Console.WriteLine("sb.ToString()");
	sb.Replace("2", "Invisible War");
	Console.WriteLine(sb.ToString());
	Console.WriteLine("sb has {0} chars.", sb.Length);
	Console.WriteLine();
}

static int Add(int x, int y) {
	return x + y;
}

static void NarrowingAttempt() {
	byte myByte = 0;
	int myInt = 200;

	// Explicitly cast the int into a byte (no loss of data).
	myByte = (byte) myInt;
	Console.WriteLine("Value of myByte: {0}", myByte);

	Console.WriteLine();
}

static void ProcessBytes() {
	Console.WriteLine("=> Using the checked keyword:");

	byte b1 = 100;
	byte b2 = 250;

	try {
		byte sum = checked((byte) Add(b1, b2));
		Console.WriteLine("sum = {0}", sum);
	} catch (OverflowException ex) {
		Console.WriteLine(ex.Message);
	} finally {
		Console.WriteLine();
	}
}