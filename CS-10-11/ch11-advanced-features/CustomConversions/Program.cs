﻿using CustomConversions;

Console.WriteLine("***** Fun with Conversions *****\n");
// Make a Rectangle
Rectangle r = new Rectangle(15, 4);
Console.WriteLine(r.ToString());
r.Draw();

Console.WriteLine();

// Convert r to a Square, based on the height of the Rectangle
Square s = (Square) r;
Console.WriteLine(s.ToString());
s.Draw();

// Converting an int to a Square.
Square sq2 = (Square) 90;
Console.WriteLine("sq2 = {0}", sq2);

// Converting a Square to an int.
int side = (int) sq2;
Console.WriteLine("Side length of sq2 = {0}", side);

Console.ReadLine();