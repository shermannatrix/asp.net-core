namespace SimpleClassExample;

class Motorcycle {
	public int driverIntensity;
	public string driverName;

	// Constructor chaining.
	public Motorcycle(): this(0, "") {
		Console.WriteLine("In default constructor.");
	}
	public Motorcycle(int intensity) : this(intensity, "") {
		Console.WriteLine("In constructor taking an int");
	}
	public Motorcycle(string name) : this(0, name) {
		Console.WriteLine("In constructor taking a string.");
	}

	// Now we've added optional arguments.
	public Motorcycle (int intensity = 0, string name = "") {
		Console.WriteLine("In main constructor");
		if (intensity > 10) {
			intensity = 10;
		}
		driverIntensity = intensity;
		driverName = name;
	}

	public void SetDriverName(string name) {
		// These two statements are functionally the same;
		// driverName = name;
		this.driverName = name;
	}

	public void PopAWheely() {
		for (int i = 0; i <= driverIntensity; i++) {
			Console.WriteLine("Yeeeeee Haaaaeeeeewww!");
		}
	}
}