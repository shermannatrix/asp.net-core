﻿// See https://aka.ms/new-console-template for more information
using FunWithRecords;

Console.WriteLine("Fun with Records!");

// Use object initialization
CarRecord myCarRecord = new CarRecord {
	Make = "Honda",
	Model = "Pilot",
	Color = "Blue"
};

Console.WriteLine("My car: ");
DisplayCarRecordStats(myCarRecord);
Console.WriteLine();

// Use the custom constructor
CarRecord anotherMyCarRecord = new CarRecord("Honda", "Pilot", "Blue");
Console.WriteLine("Another variable for my car: ");
Console.WriteLine(anotherMyCarRecord.ToString());
Console.WriteLine();
// myCarRecord.Deconstruct(out string make, out string model, out string color);
// Console.WriteLine($"Make: {make} Model: {model} Color: {color}");
Console.WriteLine($"CarRecords are the same? {myCarRecord.Equals(anotherMyCarRecord)}");
Console.WriteLine($"CarRecords are the same reference? {ReferenceEquals(myCarRecord, anotherMyCarRecord)}");
Console.WriteLine($"CarRecords are the same? {myCarRecord == anotherMyCarRecord}");
Console.WriteLine($"CarRecords are not the same? {myCarRecord != anotherMyCarRecord}");

Console.WriteLine("\nCopying Record Types Using with Expressions:\n");
CarRecord carRecordCopy = anotherMyCarRecord;
Console.WriteLine("Car Record copy results");
Console.WriteLine($"CarRecords are the same? {carRecordCopy.Equals(anotherMyCarRecord)}");
Console.WriteLine($"CarRecords are the same? {ReferenceEquals(carRecordCopy, anotherMyCarRecord)}");

CarRecord ourOtherCar = myCarRecord with { Model = "Odyssey" };
Console.WriteLine("\nMy copied car:");
Console.WriteLine(ourOtherCar.ToString());
Console.WriteLine("Car Record copy using with expression results");
Console.WriteLine($"CarRecords are the same? {ourOtherCar.Equals(myCarRecord)}");
Console.WriteLine($"CarRecords are the same? {ReferenceEquals(ourOtherCar, myCarRecord)}");

Console.ReadLine();

static void DisplayCarStats (Car c) {
	Console.WriteLine("Car Make: {0}", c.Make);
	Console.WriteLine("Car Model: {0}", c.Model);
	Console.WriteLine("Car Color: {0}", c.Color);
}

static void DisplayCarRecordStats (CarRecord c) {
	Console.WriteLine("Car Make: {0}", c.Make);
	Console.WriteLine("Car Model: {0}", c.Model);
	Console.WriteLine("Car Color: {0}", c.Color);
}
