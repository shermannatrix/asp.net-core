﻿using StaticDataAndMembers;

Console.WriteLine("***** Fun with Static Data *****\n");
SavingsAccount s1 = new SavingsAccount(50);

// Print the current interest rate.
Console.WriteLine("Interest rate is: {0}", SavingsAccount.GetInterestRate());

// Try to change the interest rate via property.
SavingsAccount.SetInterestRate(0.08);

SavingsAccount s2 = new SavingsAccount(100);

Console.WriteLine("Interest Rate is: {0}", SavingsAccount.GetInterestRate());

TimeUtilClass.PrintDate();
TimeUtilClass.PrintTime();

Console.ReadLine();
