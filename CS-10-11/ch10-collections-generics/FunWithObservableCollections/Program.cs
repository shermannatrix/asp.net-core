﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using FunWithObservableCollections;

// Make a collection to observe and add a few Person objects
ObservableCollection<Person> people = new ObservableCollection<Person>() {
	new Person { FirstName = "Peter", LastName = "Murphy", Age = 52 },
	new Person { FirstName = "Kevin", LastName = "Key", Age = 48 },
};

// Wire up the CollectionChanged event.
people.CollectionChanged += people_CollectionChanged;

// Now add a new item.
people.Add(new Person("Fred", "Smith", 32));
// Remove an item.
people.RemoveAt(0);

static void people_CollectionChanged (object sender, 
	NotifyCollectionChangedEventArgs eventArgs) {
	// What was the action that caused the event?
	Console.WriteLine($"Action for this event: {eventArgs.Action}");

	// They removed something.
	if (eventArgs.Action == NotifyCollectionChangedAction.Remove) {
		Console.WriteLine("Here are the OLD items:");
		foreach (Person p in eventArgs.OldItems) {
			Console.WriteLine(p.ToString());
		}
		Console.WriteLine();
	}

	// They added something.
	if (eventArgs.Action == NotifyCollectionChangedAction.Add) {
		// Now show the NEW items that were inserted.
		Console.WriteLine("Here are the NEW items:");
		foreach (Person p in eventArgs.NewItems) {
			Console.WriteLine(p.ToString());
		}
	}
}
