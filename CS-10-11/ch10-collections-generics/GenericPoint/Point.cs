namespace GenericPoint;

public struct Point<T> {
	// Generic state data.
	private T _xPos;
	private T _yPos;

	// Generic constructor
	public Point(T xVal, T yVal) {
		_xPos = xVal;
		_yPos = yVal;
	}

	// Generic properties
	public T X {
		get => _xPos;
		set => _xPos = value;
	}

	public T Y {
		get => _yPos;
		set => _yPos = value;
	}

    public override string ToString() 
		=> $"[{_xPos}, {_yPos}]";

	public void ResetPoint() {
		_xPos = default;
		_yPos = default;
	}
}