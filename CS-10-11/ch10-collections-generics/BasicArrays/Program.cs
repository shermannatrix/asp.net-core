﻿// Make an array of string data.
string[] strArray = { "First", "Second", "Third" };

// Show number of items in array using Length property.
Console.WriteLine("This array has {0} items.", strArray.Length);
Console.WriteLine();

// Display contents using enumerator.
foreach (string s in strArray) {
	Console.WriteLine("Array Entry: {0}", s);
}
Console.WriteLine();

// Reverse the array and print again.
Array.Reverse(strArray);
foreach (string s in strArray) {
	Console.WriteLine("Array Entry: {0}", s);
}

Console.ReadLine();
