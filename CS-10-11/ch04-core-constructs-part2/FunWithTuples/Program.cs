﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("***** Fun with Tuples *****");

(string, int, string) values = ("a", 5, "c");

Console.WriteLine($"First item: {values.Item1}");
Console.WriteLine($"Second item: {values.Item2}");
Console.WriteLine($"Third item: {values.Item3}");

(string FirstLetter, int TheNumber, string SecondLetter) valuesWithNames = ("a", 5, "c");

Console.WriteLine($"\nFirst item: {valuesWithNames.FirstLetter}");
Console.WriteLine($"Second item: {valuesWithNames.TheNumber}");
Console.WriteLine($"Third item: {valuesWithNames.SecondLetter}");

Console.WriteLine("\n=> Inferred Tuple Names");
var foo = new { Prop1 = "first", Prop2 = "second" };
var bar = (foo.Prop1, foo.Prop2);
Console.WriteLine($"{bar.Prop1};{bar.Prop2}");

Console.WriteLine("\n=> Tuples Equality/Inequality:");
// lifted conversions
var left = (a: 5, b: 10);
(int? a, int? b) nullableMembers = (5, 10);
Console.WriteLine(left == nullableMembers);	// Also true
// converted type of left is (long, long)
(long a, long b) longTuple = (5, 10);
Console.WriteLine(left == longTuple);	// Also true
// comparisons performed on (long, long) tuples
(long a, int b) longFirst = (5, 10);
(int a, long b) longSecond = (5, 10);
Console.WriteLine(longFirst == longSecond);	// Also true

Console.WriteLine("\n=> Returning tuples from a function:");
var samples = FillTheseValues();
Console.WriteLine($"Int is: {samples.a}");
Console.WriteLine($"String is: {samples.b}");
Console.WriteLine($"Boolean is: {samples.c}");

Console.WriteLine("\n=> Understanding Discards with Tuples:");
var (first, _, last) = SplitNames("Sherman WZ Chen");
Console.WriteLine($"{first}, {last}");

// Deconstructing tuples
(int X, int Y) myTuple = (4, 5);
int x = 0;
int y = 0;
(x, y) = myTuple;
Console.WriteLine($"X is: {x}");
Console.WriteLine($"Y is: {y}");
(int x1, int y1) = myTuple;
Console.WriteLine($"x1 is: {x}");
Console.WriteLine($"y1 is: {y}");

Point p = new Point(7, 5);
var pointValues = p.Deconstruct();
Console.WriteLine($"X is: {pointValues.XPos}");
Console.WriteLine($"Y is: {pointValues.YPos}");

Console.WriteLine("\n=> Deconstructing tuples with positional pattern matching:");
Point p2 = new Point(8, 3);
int xp2 = 0;
int yp2 = 0;
(xp2, yp2) = p2;
Console.WriteLine($"XP2 is: {xp2}");
Console.WriteLine($"YP2 is: {yp2}");

Console.WriteLine($"This is quadrant {GetQuadrant2(new Point(0, -1))}");

Console.ReadLine();

static (int a, string b, bool c) FillTheseValues() {
	return (9, "Enjoy your string", true);
}

static (string first, string middle, string last) SplitNames(string fullName) {
	return ("Sherman", "WZ", "Chen");
}

// Switch expression with Tuples
static string RockPaperScissors(string first, string second) {
	return (first, second) switch {
		("rock", "paper") => "Paper wins.",
		("rock", "scissors") => "Rock wins.",
		("paper", "rock") => "Paper wins.",
		("paper", "scissors") => "Scissors wins.",
		("scissors", "rock") => "Rock wins.",
		("scissors", "paper") => "Scissors wins.",
		(_,_) => "Tie.",
	};
}

static string GetQuadrant2(Point p) {
	return p switch {
		(0, 0) => "Origin",
		var (x, y) when x > 0 && y > 0 => "One",
		var (x, y) when x < 0 && y > 0 => "Two",
		var (x, y) when x < 0 && y < 0 => "Three",
		var (x, y) when x > 0 && y < 0 => "Four",
		var (_, _) => "Border",
	};
}

struct Point {
	// Fields of the structure.
	public int X;
	public int Y;

	// A custom constructor.
	public Point (int XPos, int YPos) {
		X = XPos;
		Y = YPos;
	}

	public (int XPos, int YPos) Deconstruct() => (X, Y);

	public void Deconstruct(out int XPos, out int YPos) =>
		(XPos, YPos) = (X, Y);
}