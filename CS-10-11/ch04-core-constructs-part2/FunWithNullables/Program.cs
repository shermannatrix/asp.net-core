﻿// See https://aka.ms/new-console-template for more information
using System;

Console.WriteLine("***** Fun with Nullable Value Types *****\n");
DatabaseReader dr = new DatabaseReader();

// Get int from "database".
int? i = dr.GetIntFromDatabase();

if (i.HasValue) {
	Console.WriteLine("Value of 'i' is: {0}", i.Value);
} else {
	Console.WriteLine("Value of 'i' is undefined.");
}

// Get bool from "database".
bool? b = dr.GetBoolFromDatabase();
if (b != null) {
	Console.WriteLine("Value of 'b' is: {0}", b.Value);
} else {
	Console.WriteLine("Value of 'b' is undefined.");
}

// string? nullableString = null;
// TestClass? myNullableClass = null;

// #nullable disable
// TestClass anotherNullableClass = null;

// EnterLogData(null);

// If th value from GetIntFromDatabase() is null, assign
// local variable to 100.
int myData = dr.GetIntFromDatabase() ?? 100;
Console.WriteLine("Value of myData: {0}", myData);

// Null-coalescing assignment operator
int? nullableInt = null;
nullableInt ??= 12;
nullableInt ??= 24;
Console.WriteLine(nullableInt);
TesterMethod(null);

Console.ReadLine();

static void LocalNullableVars() {
	// Define some local nullable variables.
	int? nullableInt = 10;
	double? nullableDouble = 3.14;
	bool? nullableBool = null;
	char? nullableChar = 'a';
	int?[] arrOfNullableInts = new int?[10];
}

static void LocalVarsUsingNullable() {
	// Define some local nullable types using Nullable<T>
	Nullable<int> nullableInt = 10;
	Nullable<double> nullableDouble = 3.14;
	Nullable<bool> nullableBool = null;
	Nullable<char> nullableChar = 'a';
	Nullable<int>[] arrOfNullableInts = new Nullable<int>[10];
}

static void EnterLogData(string message, string owner = "Programmer") {
	ArgumentNullException.ThrowIfNull(message);
	Console.WriteLine("Error: {0}", message);
	Console.WriteLine("Owner of Error: {0}", owner);
}

static void TesterMethod (string[] args) {
	// We should check for null before accessing the array data
	Console.WriteLine($"You sent me {args?.Length ?? 0} arguments.");
}

class DatabaseReader {
	// Nullable data field.
	public int? numericValue = null;
	public bool? boolValue = true;

	// Note the nullable return type.
	public int? GetIntFromDatabase() {
		return numericValue;
	}

	// Note the nullable return type.
	public bool? GetBoolFromDatabase() {
		return boolValue;
	}
}

public class TestClass {
	public string Name { get; set; }
	public int Age { get; set; }
}