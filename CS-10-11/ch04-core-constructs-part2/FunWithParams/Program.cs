﻿// Passing ref-types by value.
Console.WriteLine("***** Passing Person object by value *****");
Person fred = new Person("Fred", 12);
Console.WriteLine("\nBefore by value call, Person is:");
fred.Display();

SendAPersonByValue(fred);
Console.WriteLine("\nAfter by value call, Person is:");
fred.Display();

Console.WriteLine();

Console.WriteLine("***** Passing Person object by reference *****");
Person mel = new Person("Mel", 23);
Console.WriteLine("Before by ref call, Person is:");
mel.Display();

SendAPersonByRef(ref mel);
Console.WriteLine("After by ref call, Person is:");
mel.Display();

Console.ReadLine();

static void SendAPersonByValue(Person p) {
	// Change the age of "p"?
	p.personAge = 99;

	// With the caller see this reassignment?
	p = new Person("Nikki", 99);
}

static void SendAPersonByRef(ref Person p) {
	// Change some data of "p".
	p.personAge = 555;

	// "p" is now pointing to a new object on the heap!
	p = new Person("Nikki", 56);
}

class Person {
	public string personName;
	public int personAge;

	// Constructors
	public Person(string name, int age) {
		personName = name;
		personAge = age;
	}

	public Person() {}

	public void Display() {
		Console.WriteLine($"Name: {personName}, Age: {personAge}");
	}
}
