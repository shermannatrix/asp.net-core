﻿// See https://aka.ms/new-console-template for more information
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks.Dataflow;

Console.WriteLine("***** Fun with Methods *****\n");

// Pass two variables in by value.
int x = 9, y = 10;

AddUsingOutParam(90, 90, out int ans);
FillTheseValues(out int i, out string str, out bool b);

Console.WriteLine("Before call: X: {0}, Y: {1}", x, y);
Console.WriteLine("Answer is: {0}", Add(x, y));
Console.WriteLine("After call: X: {0}, Y: {1}", x, y);
Console.WriteLine("90 + 90 = {0}", ans);

Console.WriteLine("Int is: {0}", i);
Console.WriteLine("String is: {0}", str);
Console.WriteLine("Boolean is: {0}", b);

string str1 = "Flip";
string str2 = "Flop";
Console.WriteLine("\nBefore: {0}, {1}", str1, str2);
SwapStrings(ref str1, ref str2);
Console.WriteLine("After: {0}, {1}", str1, str2);

// Pass in comma-delimited list of doubles
double average;

average = CalculateAverage(4.0, 3.2, 5.7, 64.22, 87.2);
Console.WriteLine("\nAverage of data is: {0}", average);

// ...or pass an array of doubles.
double[] data = { 4.0, 3.2, 5.7 };
average = CalculateAverage(data);
Console.WriteLine("Average of data: {0}", average);

// Average of 0 is 0
Console.WriteLine("Average of data is: {0}", CalculateAverage());

EnterLogData("Oh no! Grid can't find data");
EnterLogData("Oh no! I can't find the payroll data", "CFO");

DisplayFancyMessage(message: "Wow! Very fancy indeed",
	textColor: ConsoleColor.DarkRed, 
	backgroundColor: ConsoleColor.White);

DisplayFancyMessage(backgroundColor: ConsoleColor.Green,
	message: "Testing...",
	textColor: ConsoleColor.DarkBlue);

// This is OK, as position args are listed before named args.
DisplayFancyMessage(ConsoleColor.Blue,
	message: "Testing...",
	backgroundColor: ConsoleColor.White);

// This is OK, all arguments are in the correct order.
DisplayFancyMessage(textColor: ConsoleColor.White,
	backgroundColor: ConsoleColor.Blue,
	"Testing...");

Console.ReadLine();

static int AddWrapper(int x, int y) {
	// Do some validation here
	return Add();

	int Add() {
		return x + y;
	}
}

// C# 9 updated local functions to allow for adding attributes to a 
// local function, its parameters, and its type parameters.

#nullable enable
static void Process(string?[] lines, string mark) {
	foreach(var line in lines) {
		if (IsValid(line)) {
			// Processing Logic
		}
	}

	bool IsValid([NotNullWhen(true)] string? line) {
		return !string.IsNullOrEmpty(line) && line.Length >= mark.Length;
	}
}

static int AddWrapperWithSideEffect(int x, int y) {
	// Do some validation here.
	return Add();

	int Add() {
		x += 1;
		return x + y;
	}
}

static int AddWrapperWithStatic(int x, int y) {
	// Do some validation.
	return Add(x, y);

	static int Add(int x, int y) {
		return x + y;
	}
}

// Value type arguments are passed by value by default
static int Add(int x, int y) {
	int ans = x + y;
	// Caller will not see these changes as you are
	// modifying a copy of the original data.
	x = 10000;
	y = 88888;
	return ans;
}

// Output parameters must be assigned by the called method.
static void AddUsingOutParam(int x, int y, out int ans) {
	ans = x + y;
}

// Returning multiple output parameters.
static void FillTheseValues(out int a, out string b, out bool c) {
	a = 9;
	b = "Enjoy your string.";
	c = true;
}

// Reference parameters
static void SwapStrings (ref string s1, ref string s2) {
	string tempStr = s1;
	s1 = s2;
	s2 = tempStr;
}

// Return average of "some number" of doubles.
static double CalculateAverage(params double[] values) {
	Console.WriteLine("You sent me {0} doubles.", values.Length);

	double sum = 0;
	if (values.Length == 0) {
		return sum;
	}

	for (int i = 0; i < values.Length; i++) {
		sum += values[i];
	}

	return (sum / values.Length);
}

static void EnterLogData (string message, string owner = "Programmer") {
	Console.WriteLine("Error: {0}", message);
	Console.WriteLine("Owner of Error: {0}", owner);
	Console.WriteLine();
}

// Example of working with named arguments.
static void DisplayFancyMessage(ConsoleColor textColor, 
	ConsoleColor backgroundColor,
	string message) {
	// Store old colors to restore after message is printed.
	ConsoleColor oldTextColor = Console.ForegroundColor;
	ConsoleColor oldBackgroundColor = Console.BackgroundColor;

	// Set new colors and print message.
	Console.ForegroundColor = textColor;
	Console.BackgroundColor = backgroundColor;
	Console.WriteLine(message);

	// Restore previous colors.
	Console.ForegroundColor = oldTextColor;
	Console.BackgroundColor = oldBackgroundColor;
	Console.WriteLine();
}