﻿using System;

Console.WriteLine("***** Fun with Enums *****");

// Make an EmpType variable
EmpType emp = EmpType.Contractor;
AskForBonus(emp);

// Print storage for the enum.
// Console.WriteLine("EmpType uses a {0} for storage", 
//  	Enum.GetUnderlyingType(emp.GetType()));

// We can also use the typeof() operator
// Console.WriteLine("EmpType uses a {0} for storage", 
//  	Enum.GetUnderlyingType(typeof(EmpType)));

// Using the ToString() method.
// Console.WriteLine("emp is a {0}.", emp.ToString());

EmpType e2 = EmpType.Contractor;

// These types are enums in the System namespace.
DayOfWeek day = DayOfWeek.Monday;
ConsoleColor cc = ConsoleColor.Gray;

EvaluateEnum(e2);
EvaluateEnum(day);
EvaluateEnum(cc);

Console.ReadLine();

static void AskForBonus(EmpType e) {
	switch (e) {
		case EmpType.Manager:
			Console.WriteLine("How about stock options instead?");
			break;
		case EmpType.Grunt:
			Console.WriteLine("You have got to be kidding...");
			break;
		case EmpType.Contractor:
			Console.WriteLine("You already get enough cash...");
			break;
		case EmpType.VicePresident:
			Console.WriteLine("Very GOOD, Sir!");
			break;
	}
}

// This method will print out the details of any enum.
static void EvaluateEnum(System.Enum e) {
	Console.WriteLine("=> Information about {0}", e.GetType().Name);

	Console.WriteLine("Underlying storage type: {0}", 
		Enum.GetUnderlyingType(e.GetType()));
	
	// Get all name-value pais for incoming parameter.
	Array enumData = Enum.GetValues(e.GetType());
	Console.WriteLine("This enum has {0} members.", enumData.Length);

	// Now show the string name and associated value, using the D format.
	for (int i = 0; i < enumData.Length; i++) {
		Console.WriteLine("Name: {0}, Value: {0:D}", enumData.GetValue(i));
	}
}

// A custom enumeration.
enum EmpType {
	Manager,
	Grunt,
	Contractor,
	VicePresident
}
