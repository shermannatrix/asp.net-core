using System.Collections;

namespace ComparableCar;

class Radio {
	public void TurnOn (bool on) {
		Console.WriteLine(on ? "Jamming" : "Quiet time...");
	}
}

class Car : IComparable {
	// Constant for maximum speed.
	public const int MaxSpeed = 100;

	// Car properties.
	public int CurrentSpeed { get; set; } = 0;
	public string PetName { get; set; } = "";

	// Is the car still operational?
	private bool _carIsDead;

	// A car has-a radio.
	private readonly Radio _theMusicBox = new Radio();

	public int CarID { get; set; }

	// Constructors
	public Car() {}
	public Car(string name, int speed, int id) {
		CurrentSpeed = speed;
		PetName = name;
		CarID = id;
	}

	public void CrankTunes (bool state) {
		// Delegate request to inner object.
		_theMusicBox.TurnOn(state);
	}

	// See if Car has overheated.
	public void Accelerate(int delta) {
		if (_carIsDead) {
			Console.WriteLine($"{PetName} is out of order...");
		} else {
			CurrentSpeed += delta;
			if (CurrentSpeed >= MaxSpeed) {
				
				CurrentSpeed = 0;
				_carIsDead = true;

				// Use the "throw" keyword to raise an exception.
				throw new Exception($"{PetName} has overheated!"){
					HelpLink = "http://www.CarsRUs.com",
					Data = {
						{"TimeStamp", $"The car exploded at {DateTime.Now}"},
						{"Cause", "You have a lead foot."}
					}
				};
			} else {
				Console.WriteLine($"=> CurrentSpeed = {CurrentSpeed}");
			}
		}
	}

	int IComparable.CompareTo(object? obj) {
		if (obj is Car temp) {
			return this.CarID.CompareTo(temp.CarID);
		}
		throw new ArgumentException("Parameter is not a Car!");
	}

	// Property to return the PetNameComparer.
	public static IComparer SortByPetName
		=> (IComparer) new PetNameComparer();
}