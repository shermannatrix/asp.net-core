namespace CustomInterfaces;

public interface ICloneable {
	// Only derived types can support this "polymorphic interface".
	// Classes in other hierarchies have no access to this abstract member.
	object Clone();
}