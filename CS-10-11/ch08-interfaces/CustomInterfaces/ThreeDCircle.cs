// This class extends Circle and hides the inherited Draw() method.
using CustomInterfaces;

class ThreeDCircle : Circle, IDraw3D {

	public new string PetName { get; set; }
	public new void Draw() {
		Console.WriteLine("Drawing a 3D Circle");
	}

    public void Draw3D() => Console.WriteLine("Drawing Hexagon in 3D!");
}