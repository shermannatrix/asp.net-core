namespace CustomInterfaces;

class Hexagon : Shape, IDraw3D, IPointy {
	public Hexagon () {}
	public Hexagon (string name) : base (name) {}
	public override void Draw() {
		Console.WriteLine("Drawing {0} the Hexagon", PetName);
	}

	// IPointy implementation
	public byte Points => 6;

	public void Draw3D() 
		=> Console.WriteLine("Drawing Hexagon in 3D!");
}