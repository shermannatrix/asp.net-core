﻿using System.Collections;
using System.Security.Cryptography;
using CustomEnumerator;

Console.WriteLine("***** Fun with the Yield Keyword *****");
Garage carLot = new Garage();

// Get items using GetEnumerator().
foreach (Car c in carLot) {
	Console.WriteLine($"{c.PetName} is going {c.CurrentSpeed} MPH");
}

Console.WriteLine();

// Get items (in reverse!) using named iterator.
foreach (Car c in carLot.GetTheCars(true)) {
	Console.WriteLine($"{c.PetName} is going {c.CurrentSpeed} MPH");
}

// Console.WriteLine("***** Fun with IEnumerable / IEnumerator *****");
// Garage carLot = new Garage();

// // Hand over each car in the collection?
// foreach (Car c in carLot) {
// 	Console.WriteLine($"{c.PetName} is going {c.CurrentSpeed} MPH");
// }

// try {
// 	var carEnumerator = carLot.GetEnumerator();
// } catch (Exception e) {
// 	Console.WriteLine($"Exception occurred on GetEnumerator");
// }

// Manually work with IEnumerator
// IEnumerator carEnumerator = carLot.GetEnumerator();
// carEnumerator.MoveNext();
// Car myCar = (Car) carEnumerator.Current;

// Console.WriteLine($"{myCar.PetName} is going {myCar.CurrentSpeed} MPH");

Console.ReadLine();
