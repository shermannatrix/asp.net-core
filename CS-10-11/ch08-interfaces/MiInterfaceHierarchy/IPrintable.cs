namespace MiInterfaceHierarchy;
interface IPrintable {
	void Print();
	void Draw();
}